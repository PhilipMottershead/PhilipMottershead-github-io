---
title: "Introduction to my Major Minor Project"
date: 2020-01-26T13:16:00
categories:
  - blog
tags:
  - mmp
  - update
---

Below is the text that accompanied the topic for my MMP project brief.


>What do MPs do? Analysing Hansard data (suggested by Amanda Clare - afc)
>
>Description:
>
>Parliament has written records of debates between MPs and between members of the House of Lords. It keeps records on how they voted, and what they said. It keeps this data since 1803, as XML (see hansard-archive at UK parliament) and you can inspect and process it. How has language usage changed over the years in Parliament? How have the issues that have been debated changed? Were MPs more polite back then? Can we tell from the language what part of the country the MP represents? How about the party they represent? Have debates about energy changed over the years? About schools?
>
>Any project using this data will expand your-- data processing skills, machine learning skills, perhaps some statistics, and ideally,>you would have an interest in making politics more available and interesting to the general public.

This give quite a wide scope for a project to work in, however they all use the same data-set for analysis and preform natural language processing.

