---
title: "Welcome to My Blog"
date: 2020-01-26T13:00:00
categories:
  - blog
tags:
  - Jekyll
  - update
---

This is my first post using the Jekyll static site generator.

I'm using this due to it being the recommended tool for github pages which I'm using 
to host this blog.

This will be used to document my major project for my degree as well as other topic that I decide to write about. This is to give me a log of
work undertaken as well as how my ideas evolve over time. In the next post I will overview
my major project task. All Major project posts will contain the tag MMP which stands for 
Major Minor Project.  

This blog is using the Minimal Mistakes (https://github.com/mmistakes/minimal-mistakes) as it 
creates a clean ui for my website. 