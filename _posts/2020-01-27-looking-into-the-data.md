---
title: "Looking into the data"
date: 2020-01-27T18:40:00
categories:
  - blog
tags:
  - mmp
  - update
---
Today I decided to investigate downloading the data to get an idea of size of the data-set I will be using and how easy it to 
get.

To do this I created a simple parser in which uses manipulating the URL to download the files.

Below is a snippet of the key code used.
```java
private String[] prefixArray = { "/pdf/Commons/", "/html/Commons/" };
private String[] htmlArray = { "WestminsterHall/", "/CommonsChamber" };
private String pre = "https://hansard.parliament.uk/";

public void downloadFile(String date, DataType type) {
	try {
		String url = createURL(date, type);
		// connectionTimeout, readTimeout = 10 seconds
		FileUtils.copyURLToFile(new URL(url),
				new File("test" + date + ".html"), 10000, 10000);
		LOGGER.debug("found file at " + url);
	} catch (IOException e) {
		LOGGER.debug("No file found for " + date);
	}
}

private String createURL(String date, DataType type) {
	StringBuilder url = new StringBuilder();
	url.append(pre);
	if (type == DataType.PDF) {
		url.append(prefixArray[0]);
		url.append(date);
	} else if (type == DataType.HTML) {
		url.append(prefixArray[1]);
		url.append(date);
		url.append(htmlArray[1]);
	}
	return url.toString();
}
```
Whilst doing this I learnt a couple of things:
* The pdf should be ignored as the make up such a small amount of data 
* Downloading the entire data-set takes a fair bit of time
  * This can be sped up by multi-threading the downloader application
  * Took about an hour and t 
* They work for you has data-sets that may aid me using historic data http://parser.theyworkforyou.com
* You can download the speeches in text only format
  * For example https://hansard.parliament.uk/debates/GetDebateAsText/3c786fdf-09c4-4653-a5c2-ed75d447facc
    * Includes random hash, would need found by to parse HTML to get hash
    * May have issues around separating the different part which will be easier in HTML

I also took out a range of reading material to look at to get a gist of the current ideas in the area. These include the following:
* Foundations Of Computer Linguistics
* Natural Language processing for Online Applications
* Foundations of Statistical Natural Language Processing
* Statistical language Processing

Some of these might not be appropriate if I go down the AI based approach to Natural Language Processing as they link more to using a statistical model. 

I also set up a github repository for the work so far as well as Kanban board on JIRA to track time spent on the project and to keep track of issues.



