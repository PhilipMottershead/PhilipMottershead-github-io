---
title: "Project Outline"
date: 2020-01-29T18:00:00
categories:
  - blog
tags:
  - Jekyll
  - update
---

Following my first Meeting with my supervisor and lecture which defined the first deliverable for the project I focused on looking at the scope of the project. This needs further investigation to work out what I need to do.

The First thing I looked at was looking at a example day and trying to pick out phrases that are interesting and could link to 

The next thing I looked into is to start drafting my project outline. I downloaded the latex template at looked into the areas that need to be discussed. I am using Overleaf to edit the latex as it is a easy to use latex editor.

I also downloaded a Bibtex management tool called [Jabref](https://www.jabref.org/ "Jabref Homepage"). This allows me
to centrally store the citations. To link this to my Overleaf file I added it to this website at [here]( http://www.philipmottershead.co.uk/assets/mmp.bib "Bibtex file"). This allows me update  
