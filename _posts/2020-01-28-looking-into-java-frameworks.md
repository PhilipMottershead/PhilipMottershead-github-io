---
title: "Looking into Frameworks Part 1: Intro and OpenNPL"
date: 2020-01-28T16:08:00
categories:
  - blog
tags:
  - mmp
  - update
---
Todays task was to start researching the capabilities of different frameworks and libraries that I could use in
this project. 

The Language choice for this project is a important choice that I need to start thinking about. Whilst there are frameworks available in most
languages there are two main language choices I'm deciding between, Python and Java.

Python has good selection of documentation and libraries in this area and is a easy language to write in.
Frameworks such as NLTK provides a wide range of capabilities and comes with large selection of corpus and lexicon resources to use.

Java has also got a selection frameworks that could be used. One reason to use java is having more experience with it. Due to the fact that most of the modules as well as industrial year experience. This allows me to produce better software due to my better understanding of the tools available. I also am able to debug code better and create better tested software.

OpenNPL

This the main framework that today that I looked into, I used the tutorial at https://www.tutorialkart.com/opennlp. This tutorial gives example of the main features of the framework. This incudes Chunking, Sentence Detection, Tokenization, Parts of Speech detection and Name detection. I experimented with these features with the pre-existing models.